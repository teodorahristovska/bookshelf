package mk.com.iwec.BookApp.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.com.iwec.BookApp.models.*;
import mk.com.iwec.BookApp.repository.BookRepository;
@Service
public class ServiceImpl implements ServiceLayer<Book>{
	@Autowired
	private BookRepository repo;
	
	@Override
	public List<Book> findAll() {
		return repo.findAll();
	}

}
