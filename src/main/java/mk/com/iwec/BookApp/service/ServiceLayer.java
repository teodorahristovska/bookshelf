package mk.com.iwec.BookApp.service;

import java.util.List;

public interface ServiceLayer<T> {
	public List<T> findAll();
}
