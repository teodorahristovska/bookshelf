package mk.com.iwec.BookApp.repository;
import mk.com.iwec.BookApp.models.*;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {

}
