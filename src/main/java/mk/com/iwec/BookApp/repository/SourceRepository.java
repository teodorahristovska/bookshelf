package mk.com.iwec.BookApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mk.com.iwec.BookApp.models.Source;

public interface SourceRepository extends JpaRepository<Source,Long>{

}
