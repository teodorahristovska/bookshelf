package mk.com.iwec.BookApp.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mk.com.iwec.BookApp.models.*;

@Repository
public interface BookRepository extends JpaRepository<Book,Long>{

}
