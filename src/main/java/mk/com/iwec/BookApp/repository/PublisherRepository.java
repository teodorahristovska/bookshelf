package mk.com.iwec.BookApp.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import mk.com.iwec.BookApp.models.*;

public interface PublisherRepository extends JpaRepository<Publisher,Long>{

}
