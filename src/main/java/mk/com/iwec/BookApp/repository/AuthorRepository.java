package mk.com.iwec.BookApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mk.com.iwec.BookApp.models.Author;

public interface AuthorRepository extends JpaRepository<Author,String>{

}
