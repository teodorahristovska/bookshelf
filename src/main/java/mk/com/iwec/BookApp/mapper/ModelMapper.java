package mk.com.iwec.BookApp.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import mk.com.iwec.BookApp.dto.BookDto;
import mk.com.iwec.BookApp.models.Book;

@Component
public class ModelMapper {
	
	public List<BookDto> mapBooks(List<Book> books){
		return books.stream().map(b->mapBook(b)).collect(Collectors.toList());
	}
	
	public BookDto mapBook(Book book) {
		return new BookDto(book.getBookId(),book.getName(),book.getDescription(),book.getAuthors(),book.getCategories());
	}

}
