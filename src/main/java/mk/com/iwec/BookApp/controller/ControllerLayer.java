package mk.com.iwec.BookApp.controller;

import java.util.List;

public interface ControllerLayer<T> {
	
	public List<T> findAll();

}
