package mk.com.iwec.BookApp.controller;

import mk.com.iwec.BookApp.models.*;
import mk.com.iwec.BookApp.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/books")
public class ControllerImpl implements ControllerLayer<Book>{
	@Autowired
	private ServiceImpl service;
	@Override
	public List<Book> findAll() {
		return service.findAll();
	}
	

}
