package mk.com.iwec.BookApp.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mk.com.iwec.BookApp.models.Author;
import mk.com.iwec.BookApp.models.Category;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookDto {
	private Long bookId;
	private String name;
	private String description;
	private List<Author> authors;
	private List<Category> categories;
}
