package mk.com.iwec.BookApp.models;

import java.util.List;

import javax.persistence.*;

import lombok.*;

@Data
@Entity
@RequiredArgsConstructor
@NoArgsConstructor
public class Category {
	@Id
	@GeneratedValue
	private Long categoryId;
	
	@NonNull
	private String name;
	
	@NonNull
	@ManyToMany(mappedBy = "categories", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Book> books;
}
