package mk.com.iwec.BookApp.models;


import lombok.*;
import javax.persistence.*;

@Data
@Entity
@RequiredArgsConstructor
@NoArgsConstructor
public class Source {
	@Id 
	@GeneratedValue
	private Long sourceId;
	
	@NonNull
	private String format;
	
	@NonNull
	private String url;
	
}
