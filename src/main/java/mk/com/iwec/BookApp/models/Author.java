package mk.com.iwec.BookApp.models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import lombok.*;

@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class Author {
	@Id
	@Column(length = 13, unique = true)
	@NonNull
	private String ssn;

	@NonNull
	private String name;

	@NonNull
	private String lastName;

	@NonNull
	private Date birthDate;

	@NonNull
	@ManyToMany(mappedBy = "authors", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Book> books;
}
