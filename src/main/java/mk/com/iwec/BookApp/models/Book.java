package mk.com.iwec.BookApp.models;

import java.util.List;
import javax.persistence.*;
import lombok.*;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class Book {

	@Id
	@GeneratedValue
	private Long bookId;
	
	@NonNull
	@Column(length = 200)
	private String name;
		
	@NonNull
	private Long edition;
	
	@NonNull
	private String description;
	
	@NonNull
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn
	private List<Source> sources;
	
	@NonNull
	@ManyToOne
	@JoinColumn(name="publisherId")
	private Publisher publisher;
	
	@NonNull
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "book_from_authors", joinColumns = @JoinColumn(name = "bookId"), inverseJoinColumns = @JoinColumn(name = "ssn"))
	private List<Author> authors;
	
	@NonNull
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "book_category", joinColumns = @JoinColumn(name = "bookId"), inverseJoinColumns = @JoinColumn(name = "categoryId"))
	private List<Category> categories;
	
	

}
