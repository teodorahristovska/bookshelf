package mk.com.iwec.BookApp.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.Id;

import lombok.*;

@Data
@Entity
public class Publisher {

	@Id
	@GeneratedValue
	private Long publisherId;

	@NonNull
	private String name;

	@NonNull
	private String address;

	@OneToMany(mappedBy = "publisher")
	private List<Book> books;

}
